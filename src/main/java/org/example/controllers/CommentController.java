package org.example.controllers;

import org.example.dto.CommentDTO;
import org.example.services.CommentCRUDService;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/comment")
public class CommentController {

    private final CommentCRUDService commentCRUDService;

    public CommentController(CommentCRUDService commentCRUDService) {
        this.commentCRUDService = commentCRUDService;
    }

    @GetMapping("/{id}")
    public CommentDTO getCommentById(@PathVariable Integer id) {
        return commentCRUDService.getById(id);
    }

    @GetMapping
    public Collection<CommentDTO> getAll() {
        return commentCRUDService.getAll();
    }

    @PostMapping
    public void createComment(@RequestBody CommentDTO commentDTO) {
        commentCRUDService.create(commentDTO);
    }

    @PutMapping("/{id}")
    public void updateComment(@PathVariable Integer id, @RequestBody CommentDTO commentDTO) {
        commentDTO.setId(id);
        commentCRUDService.update(commentDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        commentCRUDService.delete(id);
    }

}
