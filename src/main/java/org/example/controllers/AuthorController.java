package org.example.controllers;

import lombok.RequiredArgsConstructor;
import org.example.dto.AuthorDTO;
import org.example.services.AuthorCRUDService;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequiredArgsConstructor
@RequestMapping("/author")
public class AuthorController {

    private final AuthorCRUDService authorCRUDService;

    @GetMapping("/{id}")
    public AuthorDTO getAuthorById(@PathVariable Integer id) {
        return authorCRUDService.getById(id);
    }

    @GetMapping
    public Collection<AuthorDTO> getAll() {
        return authorCRUDService.getAll();
    }

    @PostMapping
    public void create(@RequestBody AuthorDTO authorDTO) {
        authorCRUDService.create(authorDTO);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable Integer id, @RequestBody AuthorDTO authorDTO) {
        authorDTO.setId(id);
        authorCRUDService.update(authorDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        authorCRUDService.delete(id);
    }

}
