package org.example.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.dto.CommentDTO;
import org.example.entity.Author;
import org.example.entity.Comment;
import org.example.repositories.AuthorRepository;
import org.example.repositories.CommentRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommentCRUDService implements CRUDService<CommentDTO> {

    @Value("${comment.length.max}")
    private Integer commentMaxLength;
    private final CommentRepository commentRepository;
    private final AuthorRepository authorRepository;

    @Override
    public CommentDTO getById(Integer id) {
        log.info("get comment by id " + id);
        Comment comment = commentRepository.findById(id).orElseThrow();
        return mapToDto(comment);
    }

    @Override
    public Collection<CommentDTO> getAll() {
        log.info("get all comments");
        return commentRepository.findAll()
                .stream()
                .map(CommentCRUDService::mapToDto)
                .toList();
    }

    @Override
    public void create(CommentDTO commentDTO) {
        log.info("create comment");
        if (commentDTO.getText().length() > commentMaxLength) {
            throw new RuntimeException("max comment length: " + commentMaxLength
                    + "\ncomment length: " + commentDTO.getText().length());
        }
        Comment comment = mapToEntity(commentDTO);
        Integer authorId = commentDTO.getAuthorId();
        Author author = authorRepository.findById(authorId).orElseThrow();
        comment.setAuthor(author);
        commentRepository.save(comment);
    }

    @Override
    public void update(CommentDTO commentDTO) {
        log.info("update comment");
        if (commentDTO.getText().length() > commentMaxLength) {
            throw new RuntimeException("max comment length: " + commentMaxLength
                    + "\ncomment length: " + commentDTO.getText().length());
        }
        Comment comment = mapToEntity(commentDTO);
        Integer authorId = commentDTO.getAuthorId();
        Author author = authorRepository.findById(authorId).orElseThrow();
        comment.setAuthor(author);
        commentRepository.save(comment);
    }

    @Override
    public void delete(Integer id) {
        log.info("delete comment");
        commentRepository.deleteById(id);
    }

    public static CommentDTO mapToDto(Comment comment) {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(comment.getId());
        commentDTO.setText(comment.getText());
        commentDTO.setAuthorId(comment.getAuthor().getId());
        return commentDTO;
    }

    public static Comment mapToEntity(CommentDTO commentDTO) {
        Comment comment = new Comment();
        comment.setId(commentDTO.getId());
        comment.setText(commentDTO.getText());
        return comment;
    }

}
