package org.example.services;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.dto.AuthorDTO;
import org.example.entity.Author;
import org.example.repositories.AuthorRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthorCRUDService implements CRUDService<AuthorDTO> {

    private final AuthorRepository authorRepository;

    @Override
    public AuthorDTO getById(Integer id) {
        log.info("get author by id: " + id);
        Author author = authorRepository.findById(id).orElseThrow();
        return mapToDTO(author);
    }

    @Override
    public Collection<AuthorDTO> getAll() {
        log.info("get all authors");
        return authorRepository.findAll().stream()
                .map(AuthorCRUDService::mapToDTO)
                .toList();
    }

    @Override
    public void create(AuthorDTO authorDTO) {
        log.info("create author");
        authorRepository.save(mapToEntity(authorDTO));
    }

    @Override
    public void update(AuthorDTO authorDTO) {
        log.info("update author");
        authorRepository.save(mapToEntity(authorDTO));
    }

    @Override
    public void delete(Integer id) {
        log.info("delete author by id: " + id);
        authorRepository.deleteById(id);
    }

    public static Author mapToEntity(AuthorDTO authorDTO) {
        Author author = new Author();
        author.setId(authorDTO.getId());
        author.setFirstName(authorDTO.getFirstName());
        author.setLastName(authorDTO.getLastName());
        author.setRating(authorDTO.getRating());
        author.setComments(authorDTO.getCommentsDTO()
                .stream()
                .map(CommentCRUDService::mapToEntity)
                .toList());
        return author;
    }

    public static AuthorDTO mapToDTO(Author author) {
        AuthorDTO authorDTO = new AuthorDTO();
        authorDTO.setId(author.getId());
        authorDTO.setFirstName(author.getFirstName());
        authorDTO.setLastName(author.getLastName());
        authorDTO.setRating(author.getRating());
        authorDTO.setCommentsDTO(author.getComments()
                .stream()
                .map(CommentCRUDService::mapToDto)
                .toList());
        return authorDTO;
    }
}
