package org.example.dto;

import lombok.Data;

import java.util.List;

@Data
public class AuthorDTO {

    private Integer id;
    private String firstName;
    private String lastName;
    private Long rating;
    private List<CommentDTO> commentsDTO;

}
