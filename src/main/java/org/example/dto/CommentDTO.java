package org.example.dto;

import lombok.Data;

@Data
public class CommentDTO {

    private Integer id;
    private String text;
    private Integer authorId;

}
